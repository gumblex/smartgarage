#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import time
import random
import sqlite3
import logging
import requests
import collections
import urllib.parse
import concurrent.futures
from bs4 import BeautifulSoup

HEADERS = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Encoding": "gzip, deflate",
    "Accept-Language": "en-US,en;q=0.5",
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; rv:45.0) Gecko/20100101 Firefox/45.0",
}

logging.basicConfig(stream=sys.stderr, format='%(asctime)s [%(levelname)s] %(message)s', level=logging.INFO)

PageType = collections.namedtuple('PageType', 'node language id pagination')

def base_repr(number, base=2, padding=0):
    """
    Return a string representation of a number in the given base system.
    Parameters
    ----------
    number : int
        The value to convert. Positive and negative values are handled.
    base : int, optional
        Convert `number` to the `base` number system. The valid range is 2-36,
        the default value is 2.
    padding : int, optional
        Number of zeros padded on the left. Default is 0 (no padding).
    Returns
    -------
    out : str
        String representation of `number` in `base` system.
    See Also
    --------
    binary_repr : Faster version of `base_repr` for base 2.
    Examples
    --------
    >>> np.base_repr(5)
    '101'
    >>> np.base_repr(6, 5)
    '11'
    >>> np.base_repr(7, base=5, padding=3)
    '00012'
    >>> np.base_repr(10, base=16)
    'A'
    >>> np.base_repr(32, base=16)
    '20'

    Copied from `numpy.base_repr`.
    """
    # digits = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    digits = '0123456789abcdefghijklmnopqrstuvwxyz'
    if base > len(digits):
        raise ValueError("Bases greater than 36 not handled in base_repr.")
    elif base < 2:
        raise ValueError("Bases less than 2 not handled in base_repr.")

    num = abs(number)
    res = []
    while num:
        res.append(digits[num % base])
        num //= base
    if padding:
        res.append('0' * padding)
    if number < 0:
        res.append('-')
    return ''.join(reversed(res or '0'))

def split_unit(s):
    for i,c in enumerate(s):
        if not c.isdigit():
            break
    return int(s[:i]), s[i:]

def make_insert(d):
    keys, values = zip(*d.items())
    return ', '.join(keys), ', '.join('?' * len(values)), values

def make_update(d):
    keys, values = zip(*d.items())
    return ', '.join(k + '=?' for k in keys), values

class AVMOOCrawler:

    sitename = 'AVMOO'
    domain = 'avmo.pw'
    languages = ('ja', 'en', 'cn', 'tw')
    translations = 2

    def __init__(self, db='avmoo.db'):
        self.root = 'https://%s/' % self.domain

        self.db = sqlite3.connect(db)
        self.cur = self.db.cursor()
        self.cur.execute('PRAGMA journal_mode=WAL')
        self.session = requests.Session()
        self.session.headers.update(HEADERS)
        self.executor = concurrent.futures.ThreadPoolExecutor(1)
        self.tasks = []
        self.count = 0

    def init_db(self):
        #self.cur.execute('CREATE TABLE IF NOT EXISTS config ('
        #    'key TEXT PRIMARY KEY,'
        #    'value TEXT'
        #')')
        self.cur.execute('CREATE TABLE IF NOT EXISTS links ('
            'id INTEGER,'
            'node TEXT,'
            'lang TEXT,'
            'updated INTEGER,'
            'PRIMARY KEY (id, node, lang)'
        ')')
        self.cur.execute('CREATE TABLE IF NOT EXISTS movies ('
            'id INTEGER PRIMARY KEY,'
            'title TEXT,'
            'identifier TEXT,'
            'release_date TEXT,'
            'length INTEGER,'
            'director INTEGER,'
            'studio INTEGER,'
            'label INTEGER,'
            'series INTEGER,'
            'cover TEXT,'
            'cover_small TEXT,'
            'FOREIGN KEY(director) REFERENCES features(id),'
            'FOREIGN KEY(studio) REFERENCES features(id),'
            'FOREIGN KEY(label) REFERENCES features(id),'
            'FOREIGN KEY(series) REFERENCES features(id)'
        ')')
        self.cur.execute('CREATE TABLE IF NOT EXISTS movie_genre ('
            'movie INTEGER,'
            'genre INTEGER,'
            'PRIMARY KEY (movie, genre),'
            'FOREIGN KEY(movie) REFERENCES movies(id),'
            'FOREIGN KEY(genre) REFERENCES features(id)'
        ')')
        self.cur.execute('CREATE TABLE IF NOT EXISTS movie_star ('
            'movie INTEGER,'
            'star INTEGER,'
            'PRIMARY KEY (movie, star),'
            'FOREIGN KEY(movie) REFERENCES movies(id),'
            'FOREIGN KEY(star) REFERENCES stars(id)'
        ')')
        self.cur.execute('CREATE TABLE IF NOT EXISTS movie_snapshot ('
            'movie INTEGER,'
            'snapshot TEXT,'
            'FOREIGN KEY(movie) REFERENCES movies(id)'
        ')')
        self.cur.execute('CREATE TABLE IF NOT EXISTS stars ('
            'id INTEGER PRIMARY KEY,'
            'name TEXT,'
            'image TEXT,'
            'birthday TEXT,'
            'height INTEGER,'
            'cup TEXT,'
            'bust INTEGER,'
            'waist INTEGER,'
            'hips INTEGER,'
            'hometown TEXT,'
            'hobby TEXT'
        ')')
        # director, studio, label, series, genres
        self.cur.execute('CREATE TABLE IF NOT EXISTS features ('
            'type TEXT,'
            'id INTEGER,'
            'value TEXT,'
            'PRIMARY KEY (id, type)'
        ')')
        self.cur.execute('CREATE TABLE IF NOT EXISTS translations ('
            'type TEXT,'
            'id INTEGER,'
            'lang TEXT,'
            'value TEXT,'
            'PRIMARY KEY (type, id, lang)'
        ')')
        self.cur.execute('CREATE INDEX IF NOT EXISTS idx_movie_genre'
            ' ON movie_genre (movie)')
        self.cur.execute('CREATE INDEX IF NOT EXISTS idx_movie_star'
            ' ON movie_star (movie)')
        self.cur.execute('CREATE INDEX IF NOT EXISTS idx_movie_snapshot'
            ' ON movie_snapshot (movie)')
        self.cur.execute("DELETE FROM translations WHERE type = 'movie' "
            "AND id IN (SELECT movies.id FROM movies INNER JOIN translations "
            "ON translations.id = movies.id "
            "WHERE translations.type = 'movie' "
            "AND translations.value = movies.title)")
        self.cur.execute("DELETE FROM translations WHERE type = 'star' "
            "AND id IN (SELECT stars.id FROM stars INNER JOIN translations "
            "ON translations.id = stars.id "
            "WHERE translations.type = 'star' "
            "AND translations.value = stars.name)")
        self.cur.execute("DELETE FROM links WHERE id IN ("
            "SELECT id FROM movies WHERE title IS null) "
            "AND node='movie' AND lang=?", (self.languages[0],))
        self.db.commit()

    def initial_tasks(self):
        self.init_db()
        if not self.cur.execute('SELECT 1 FROM features LIMIT 1').fetchone():
            for lang in self.languages:
                self.process_page(*self.fetch_page(
                    PageType('genre', lang, None, None)))
            self.process_page(*self.fetch_page(
                PageType('actresses', self.languages[0], None, None)))
        self.process_page(*self.fetch_page(
            PageType('movie', self.languages[0], None, None)))
        self.enumerate_links()
        self.db.commit()

    def get_tasks(self):
        tasks = list(self.cur.execute("SELECT id, node, lang FROM links WHERE updated IS NULL ORDER BY node DESC, RANDOM() LIMIT 1000"))
        if tasks:
            tasks.reverse()
            return tasks
        else:
            return

    def do_fetch(self, task):
        pid, node, lang = task
        pagetype = PageType(node, lang, pid, None)
        try:
            return self.fetch_page(pagetype)
        except Exception:
            logging.exception('Failed to process %r' % (pagetype,))

    def enumerate_links(self):
        ids = set(row[0] for row in self.cur.execute("SELECT id FROM links WHERE node='movie'"))
        if ids:
            for i in range(1, max(ids)):
                for lang in self.languages[:self.translations]:
                    self.insert_link(PageType('movie', lang, i, None))
        #ids = set(row[0] for row in self.cur.execute("SELECT id FROM links WHERE node='star'"))
        #if ids:
            #for i in range(1, max(ids)):
                #self.insert_link(PageType('star', self.languages[0], i, None))

    def start(self):
        self.initial_tasks()
        tasks = self.get_tasks()
        while tasks:
            for result in self.executor.map(self.do_fetch, tasks):
                if result:
                    pagetype, content = result
                    r = self.process_page(pagetype, content)
                    if r is not False:
                        self.insert_link(pagetype, int(time.time()))
            tasks = self.get_tasks()
        self.db.commit()

    def parse_url(self, url):
        parseresult = urllib.parse.urlparse(urllib.parse.urljoin(self.root, url))
        path = parseresult.path.lstrip('/').split('/')
        if parseresult.netloc != self.domain:
            # type, page, lanb
            return None
        elif len(path) == 0:
            return PageType('frontpage', None, None, None)
        elif len(path) == 1:
            return PageType('movie', path[0], None, None)
        elif len(path) == 2:
            return PageType(path[1], path[0], None, None)
        elif len(path) == 3:
            if '.' in path[2]:
                assert path[1] == 'genre'
                nodeid = [int(i, 36) for i in path[2].split('.')]
            else:
                nodeid = int(path[2], 36)
            return PageType(path[1], path[0], nodeid, None)
        elif len(path) == 4:
            raise AssertionError
        elif len(path) == 5:
            assert path[3] == 'currentPage'
            return PageType(path[1], path[0], int(path[2], 36), int(path[4]))

    def unparse_url(self, pagetype):
        if pagetype.node == 'frontpage':
            return '/'
        elif (pagetype.node == 'movie' and
                not pagetype.id and not pagetype.pagination):
            return '/' + pagetype.language
        res = ['', pagetype.language, pagetype.node]
        if pagetype.id:
            res.append(base_repr(pagetype.id, 36))
        if pagetype.pagination:
            res.append('currentPage/%d' % pagetype.pagination)
        return '/'.join(res)

    def insert_feature(self, ftype, pid, lang, value):
        if isinstance(pid, int):
            pids = (pid,)
        else:
            pids = pid
        for pid in pids:
            res = self.cur.execute('SELECT id FROM features WHERE type=? AND id=?',
                    (ftype, pid)).fetchone()
            if res is None:
                self.cur.execute('INSERT INTO features (type, id) VALUES (?, ?)', (ftype, pid))
            if lang == self.languages[0]:
                self.cur.execute('UPDATE features SET value=? WHERE id=?', (value, pid))
            else:
                self.insert_translation(ftype, pid, lang, value)

    def insert_translation(self, ftype, pid, lang, value):
        if lang == self.languages[0] or not value:
            return
        if ftype == 'movie':
            res = self.cur.execute('SELECT title FROM movies WHERE id=?', (pid,)).fetchone()
        elif ftype == 'star':
            res = self.cur.execute('SELECT name FROM stars WHERE id=?', (pid,)).fetchone()
        else:
            res = self.cur.execute('SELECT value FROM features WHERE id=? AND type=?', (pid, ftype)).fetchone()
        name = res[0] if res else None
        if name != value:
            self.cur.execute('INSERT OR IGNORE INTO translations VALUES (?,?,?,?)',
                (ftype, pid, lang, value))

    def insert_movie(self, pid, lang, attributes):
        res = self.cur.execute('SELECT id FROM movies WHERE id=?', (pid,)).fetchone()
        if attributes.get('title') and lang != self.languages[0]:
            self.insert_translation('movie', pid, lang, attributes['title'])
            del attributes['title']
        if res:
            keys, values = make_update(attributes)
            self.cur.execute('UPDATE movies SET %s WHERE id=?' % keys, values + (pid,))
        else:
            attributes['id'] = pid
            keys, qms, values = make_insert(attributes)
            self.cur.execute('INSERT INTO movies (%s) VALUES (%s)' % (keys, qms), values)

    def insert_star(self, pid, lang, attributes):
        res = self.cur.execute('SELECT id FROM stars WHERE id=?', (pid,)).fetchone()
        if attributes.get('name') and lang != self.languages[0]:
            self.insert_translation('star', pid, lang, attributes['name'])
            del attributes['name']
        if res:
            keys, values = make_update(attributes)
            self.cur.execute('UPDATE stars SET %s WHERE id=?' % keys, values + (pid,))
        else:
            attributes['id'] = pid
            keys, qms, values = make_insert(attributes)
            self.cur.execute('INSERT INTO stars (%s) VALUES (%s)' % (keys, qms), values)

    def insert_link(self, pagetype, date=None):
        if date:
            self.cur.execute('REPLACE INTO links VALUES (?,?,?,?)',
                (pagetype.id, pagetype.node, pagetype.language, date))
        else:
            self.cur.execute('INSERT OR IGNORE INTO links VALUES (?,?,?,?)',
                (pagetype.id, pagetype.node, pagetype.language, None))

    def change_session(self):
        global HSession
        self.session.close()
        self.session = requests.Session()
        self.session.headers.update(HEADERS)

    def fetch_page(self, pagetype):
        url = self.unparse_url(pagetype)
        pagetype = self.parse_url(url)
        r = self.session.get(urllib.parse.urljoin(self.root, url), timeout=30)
        self.count += 1
        if self.count > random.randint(5, 20):
            self.change_session()
            self.count = 0
        if r.status_code == 200:
            logging.info(url)
            return pagetype, r.content.replace(b'\0', b'')
        elif r.status_code == 404:
            logging.info(url)
            return pagetype, None
        else:
            logging.warning('%s: %d', url, r.status_code)
            self.change_session()
            return pagetype, False

    def process_page(self, pagetype, content):
        if not content:
            return content
        soup = BeautifulSoup(content, 'html5lib')
        # director, studio, label, series, genres
        if pagetype.node == 'frontpage':
            pass
        elif pagetype.node == 'movie':
            if pagetype.id:
                self.process_movie_detail(soup, pagetype.id, pagetype.language)
            else:
                self.process_movie_index(soup, 'movie', pagetype.language)
        elif pagetype.node == 'actresses':
            self.process_star_index(soup)
        elif pagetype.node == 'star':
            self.process_star_detail(soup, pagetype.id)
            self.process_movie_index(soup, 'star', pagetype.language)
        elif pagetype.node == 'genre' and not pagetype.id:
            self.process_genre_index(soup)
        else:
            self.process_movie_index(soup, pagetype.node, pagetype.language)

    def process_movie_detail(self, soup, pid, lang):
        divinfo = soup.find("div", class_="info")
        cover = soup.find("div", class_="screencap").img['src']
        identifier = (divinfo.find("span", style="color:#CC0000;").string or '')
        if soup.title and soup.title.string:
            title = soup.title.string
            assert title.startswith(identifier + ' ')
            assert title.endswith(' - ' + self.sitename)
            title = title[len(identifier)+1:-8]
        else:
            title = None
        attributes = {'title': title, 'identifier': identifier, 'cover': cover}
        genres = []
        if cover.endswith('pl.jpg'):
            attributes['cover_small'] = cover[:-5] + 's.jpg'
        for a in divinfo.find_all('a'):
            value = (a.string or '').strip()
            pagetype = self.parse_url(a['href'])
            self.insert_feature(pagetype.node, pagetype.id, pagetype.language, value)
            if pagetype.node == 'genre':
                if isinstance(pagetype.id, int):
                    genres.append(pagetype.id)
                else:
                    genres.append(pagetype.id[0])
            else:
                attributes[pagetype.node] = pagetype.id
        ps = divinfo.find_all('p')
        try:
            attributes['release_date'] = tuple(ps[1].stripped_strings)[1]
        except IndexError:
            pass
        try:
            attributes['length'] = split_unit(tuple(ps[2].stripped_strings)[1])[0]
        except IndexError:
            pass
        for genre in genres:
            self.cur.execute('INSERT OR IGNORE INTO movie_genre VALUES (?, ?)',
                (pid, genre))
        waterfall = soup.find("div", id="sample-waterfall")
        if waterfall:
            for img in waterfall.find_all('img'):
                self.cur.execute('INSERT OR IGNORE INTO movie_snapshot VALUES (?, ?)',
                    (pid, img['src']))
        waterfall = soup.find("div", id="avatar-waterfall")
        if waterfall:
            for a in waterfall.find_all('a', class_="avatar-box"):
                pagetype = self.parse_url(a['href'])
                name = (a.span.string or '').strip()
                self.insert_link(pagetype)
                self.insert_translation('star', pagetype.id, pagetype.language, name)
                self.cur.execute('INSERT OR IGNORE INTO movie_star VALUES (?, ?)',
                    (pid, pagetype.id))
        self.insert_movie(pid, lang, attributes)

    def process_genre_index(self, soup):
        for a in soup.find_all("a", class_="col-lg-2 col-md-2 col-sm-3 col-xs-6 text-center"):
            pagetype = self.parse_url(a['href'])
            assert pagetype.node == 'genre'
            self.insert_feature('genre', pagetype.id, pagetype.language, a.string.strip())

    def process_movie_index(self, soup, node, lang):
        waterfall = soup.find('div', id='waterfall')
        for a in waterfall.find_all("a", class_="movie-box"):
            movie = self.parse_url(a['href'])
            title = next(a.span.stripped_strings)
            if movie.id:
                self.insert_link(movie)
                self.insert_translation('movie', movie.id, movie.language, title)

    def process_star_index(self, soup):
        waterfall = soup.find('div', id='waterfall')
        for a in waterfall.find_all("a", class_="avatar-box"):
            star = self.parse_url(a['href'])
            name = a.span.string.strip()
            self.insert_link(star)
            self.insert_translation('star', star.id, star.language, name)

    def process_star_detail(self, soup, pid):
        # This page should be in self.languages[0]
        divbox = soup.find('div', class_='avatar-box')
        imgurl = divbox.find('div', class_='photo-frame').img['src']
        divinfo = divbox.find('div', class_='photo-info')
        name = ((divinfo.find('span', class_='pb-10') or
                divinfo.find('span', class_='pb10')).string or '').strip()
        attributes = {'name': name, 'image': imgurl}
        for p in divinfo.find_all('p'):
            fields = p.string.strip().split(': ', 1)
            try:
                if len(fields) < 2:
                    continue
                elif fields[0] in ('Birthday', '生年月日', '生日'):
                    attributes['birthday'] = fields[1]
                elif fields[0] in ('Age', '年齢', '年齡', '年龄'):
                    pass
                elif fields[0] in ('Height', '身長', '身高'):
                    assert fields[1][-2:] == 'cm'
                    attributes['height'] = int(fields[1][:-2])
                elif fields[0] in ('Cup', 'ブラのサイズ', '罩杯'):
                    attributes['cup'] = fields[1]
                elif fields[0] in ('Bust', 'バスト', '胸圍', '胸围'):
                    assert fields[1][-2:] == 'cm'
                    attributes['bust'] = int(fields[1][:-2])
                elif fields[0] in ('Waist', 'ウエスト', '腰圍', '腰围'):
                    assert fields[1][-2:] == 'cm'
                    attributes['waist'] = int(fields[1][:-2])
                elif fields[0] in ('Hips', 'ヒップ', '臀圍', '臀围'):
                    assert fields[1][-2:] == 'cm'
                    attributes['hips'] = int(fields[1][:-2])
                elif fields[0] in ('Hometown', '出身地', '出生地'):
                    attributes['hometown'] = fields[1]
                elif fields[0] in ('Hobby', '趣味', '愛好', '爱好'):
                    attributes['hobby'] = fields[1]
            except (AssertionError, ValueError):
                logging.exception('Actress %s %s: %s' % (base_repr(pid, 36), name, p.string.strip()))
        self.insert_star(pid, self.languages[0], attributes)

    def shutdown(self):
        self.executor.shutdown()
        self.db.commit()

class AVSOXCrawler(AVMOOCrawler):
    sitename = 'AVSOX'
    domain = 'avso.pw'
    languages = ('ja', 'en', 'cn', 'tw')
    translations = 2

class AVMEMOCrawler(AVMOOCrawler):
    sitename = 'AVMEMO'
    domain = 'avxo.pw'
    languages = ('en', 'cn', 'ja', 'tw')
    translations = 1

if __name__ == '__main__':
    if len(sys.argv) > 1:
        if sys.argv[1] == 'avsox':
            ac = AVSOXCrawler('avsox.db')
        else:
            ac = AVMEMOCrawler('avmemo.db')
    else:
        ac = AVMOOCrawler()
    try:
        ac.start()
    finally:
        ac.shutdown()
