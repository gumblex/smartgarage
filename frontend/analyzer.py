#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import json
import jieba
import zhconv
from tinysegmenter import TinySegmenter

import whoosh.analysis
from whoosh.lang import stopwords_for_language

CHINESE_STOP_WORDS = frozenset(
    '的一是了在有我他这个们来为和地到以要就可也你对能而'
    '那得于着自之过用所多经么如同当起其她本但因从者它与'
)
PUNCT = frozenset(
    '''!"\'(),-.:;?[]{}¢£¥·ˇˉ―‖‘’“”•′‵、。々〈〉《》「」『』'''
    '''【】〔〕〖〗〝〞︰︱︳︴︵︶︷︸︹︺︻︼︽︾︿﹀﹁﹂﹃﹄'''
    '''﹏﹐﹒﹔﹕﹖﹗﹙﹚﹛﹜﹝﹞！（），．：；？［｛｜｝～､￠￡￥'''
)
WHITESPACE = frozenset(' \t\n\r\x0b\x0c\u3000')

STOP_WORDS_ZH = CHINESE_STOP_WORDS | whoosh.analysis.STOP_WORDS | PUNCT | WHITESPACE
STOP_WORDS_JA = whoosh.analysis.STOP_WORDS | PUNCT | WHITESPACE

def uniq(seq, key=None): # Dave Kirby
    # Order preserving
    seen = set()
    if key:
        return [x for x in seq if key(x) not in seen and not seen.add(key(x))]
    else:
        return [x for x in seq if x not in seen and not seen.add(x)]

class ChineseConversionFilter(whoosh.analysis.Filter):
    def __init__(self, locale):
        self.locale = locale

    def __call__(self, tokens):
        for t in tokens:
            if self.locale:
                t.text = zhconv.convert(t.text, self.locale)
            yield t

class ChineseTokenizer(whoosh.analysis.Tokenizer):

    def __call__(self, text, **kargs):
        words = jieba.tokenize(text, mode="search")
        token = whoosh.analysis.Token()
        for (w, start_pos, stop_pos) in words:
            token.original = token.text = w
            token.pos = start_pos
            token.startchar = start_pos
            token.endchar = stop_pos
            yield token

def ChineseAnalyzer(stoplist=STOP_WORDS_ZH, locale='zh-hans', minsize=1):
    return (ChineseTokenizer() | whoosh.analysis.LowercaseFilter() |
            ChineseConversionFilter(locale) |
            whoosh.analysis.StopFilter(stoplist=stoplist, minsize=minsize))

class JapaneseTokenizer(whoosh.analysis.Tokenizer):

    def __init__(self):
        self.ts = TinySegmenter()

    def __call__(self, text, **kargs):
        token = whoosh.analysis.Token()
        pos = 0
        for k, word in enumerate(self.ts.tokenize(text)):
            token.original = token.text = word
            token.pos = k
            token.startchar = pos
            pos += len(word)
            token.endchar = pos
            yield token

class SelectiveJapaneseTokenizer(JapaneseTokenizer):

    def __init__(self):
        self.ts = TinySegmenter()
        self.rt = whoosh.analysis.RegexTokenizer(re.compile('\\w+(\\.?\\w+)*'))

    def __call__(self, text, **kargs):
        if all(ord(c) < 256 for c in text):
            yield from self.rt(text, **kargs)
        else:
            token = whoosh.analysis.Token()
            pos = 0
            for k, word in enumerate(self.ts.tokenize(text)):
                token.original = token.text = word
                token.pos = k
                token.startchar = pos
                pos += len(word)
                token.endchar = pos
                yield token

def JapaneseAnalyzer(stoplist=STOP_WORDS_JA, minsize=1):
    return (JapaneseTokenizer() | whoosh.analysis.LowercaseFilter() |
            whoosh.analysis.StopFilter(stoplist=stoplist, minsize=minsize))

def IdentifierAnalyzer():
    return (whoosh.analysis.IDTokenizer() |
            whoosh.analysis.LowercaseFilter() |
            whoosh.analysis.SubstitutionFilter("[-_. ]", ""))

def get_analyzer(lang='ja'):
    if lang == 'ja':
        return JapaneseAnalyzer()
    elif lang == 'en':
        return (SelectiveJapaneseTokenizer() |
                whoosh.analysis.LowercaseFilter() |
                whoosh.analysis.StopFilter(lang='en') |
                whoosh.analysis.StemFilter(lang='en'))
    elif lang.startswith('zh') or lang in ('cn', 'tw'):
        return ChineseAnalyzer()
    else:
        return whoosh.analysis.LanguageAnalyzer(lang)

def EnJaAnalyzer():
    stoplist = frozenset(STOP_WORDS_JA | stopwords_for_language('en'))
    return (SelectiveJapaneseTokenizer() |
            whoosh.analysis.LowercaseFilter() |
            whoosh.analysis.StopFilter(stoplist=stoplist, minsize=1) |
            whoosh.analysis.StemFilter(lang='en'))

class MultiLangTitleAnalyzer(whoosh.analysis.Analyzer):
    '''not useful'''
    def __init__(self):
        self.analyzers = {l:get_analyzer(l) for l in ('ja', 'en', 'cn', 'tw')}

    def __call__(self, value, **kwargs):
        titles = uniq(json.loads(value), key=lambda x: x[1])
        for lang, text in titles:
            yield from self.analyzers[lang](text, **kwargs)

re_tag_remove = re.compile(r'\([^)]+\)')
re_tag_split = re.compile(r' - |[^\w -]+')

def split_tags(tags):
    for tag in tags:
        yield tag
        tag_rm = re_tag_remove.sub('', tag).strip()
        tag_spl = list(filter(None, re_tag_split.split(tag_rm)))
        if len(tag_spl) > 1:
            for sub in tag_spl:
                yield sub.strip()
        elif tag_rm != tag:
            yield tag_rm

split_tags_uniq = lambda tags: uniq(split_tags(tags))
