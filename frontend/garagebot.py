#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import re
import sys
import time
import json
import queue
import logging
import requests
import functools
import threading
import concurrent.futures

import search

logging.basicConfig(stream=sys.stderr, format='%(asctime)s [%(name)s:%(levelname)s] %(message)s', level=logging.DEBUG if sys.argv[-1] == '-v' else logging.INFO)

logger_botapi = logging.getLogger('botapi')

executor = concurrent.futures.ThreadPoolExecutor(5)
HSession = requests.Session()

re_mdescape = re.compile(r'([\[\*_])')
mdescape = lambda s: re_mdescape.sub(r'\\\1', s)

title_translation = {
    'length': {'en': 'Length', 'ja': '収録時間', 'cn': '长度', 'tw': '長度'},
    'release': {'en': 'Release Date', 'ja': '発売日', 'cn': '发行时间', 'tw': '發行日期'},
    'director': {'en': 'Director', 'ja': '監督', 'cn': '导演', 'tw': '導演'},
    'studio': {'en': 'Studio', 'ja': 'メーカー', 'cn': '制作商', 'tw': '製作商'},
    'label': {'en': 'Label', 'ja': 'レーベル', 'cn': '发行商', 'tw': '發行商'},
    'series': {'en': 'Series', 'ja': 'シリーズ', 'cn': '系列', 'tw': '系列'},
    'tags': {'en': 'Genre', 'ja': 'ジャンル', 'cn': '类别', 'tw': '類別'},
    'stars': {'en': 'Stars', 'ja': '出演者', 'cn': '演员', 'tw': '演員'}
}

class AttrDict(dict):

    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self

class BotAPIFailed(Exception):
    pass

def async_func(func):
    @functools.wraps(func)
    def wrapped(*args, **kwargs):
        def func_noerr(*args, **kwargs):
            try:
                func(*args, **kwargs)
            except Exception:
                logger_botapi.exception('Async function failed.')
        executor.submit(func_noerr, *args, **kwargs)
    return wrapped

def bot_api(method, **params):
    for att in range(3):
        try:
            req = HSession.post(('https://api.telegram.org/bot%s/' %
                                CFG.apitoken) + method, data=params, timeout=45)
            retjson = req.content
            ret = json.loads(retjson.decode('utf-8'))
            break
        except Exception as ex:
            if att < 1:
                time.sleep((att + 1) * 2)
            else:
                raise ex
    if not ret['ok']:
        raise BotAPIFailed(repr(ret))
    return ret['result']

@async_func
def answer(inline_query_id, results, **kwargs):
    return bot_api('answerInlineQuery', inline_query_id=inline_query_id, results=json.dumps(results), **kwargs)

def updatebotinfo():
    global CFG
    d = bot_api('getMe')
    CFG['username'] = d.get('username')

def getupdates():
    global CFG
    while 1:
        try:
            updates = bot_api('getUpdates', offset=CFG['offset'], timeout=10)
        except Exception:
            logger_botapi.exception('Get updates failed.')
            continue
        if updates:
            #logger_botapi.debug('Messages coming: %r', updates)
            CFG['offset'] = updates[-1]["update_id"] + 1
            for upd in updates:
                MSG_Q.put(upd)
        time.sleep(.2)

def parse_cmd(text: str):
    t = text.strip().replace('\xa0', ' ').split(' ', 1)
    if not t:
        return (None, None)
    cmd = t[0].rsplit('@', 1)
    if len(cmd[0]) < 2 or cmd[0][0] != "/":
        return (None, None)
    if len(cmd) > 1 and 'username' in CFG and cmd[-1] != CFG['username']:
        return (None, None)
    expr = t[1] if len(t) > 1 else ''
    return (cmd[0][1:], expr.strip())

def cmd_query(text, num=5):
    text = text.strip()
    if not text:
        return []
    text_l = text.split(' ', 1)
    lang = CFG['defaultlang']
    if len(text_l) > 1 and text_l[0] in ('ja', 'en', 'cn', 'tw'):
        lang, text = text_l
    results, pagecount, total = WP.search(text, limit=num)
    details = WP.expand_results(results, lang)
    return details, lang

def format_result(details, lang='ja'):
    if not details:
        return 'Found nothing.'
    ret = []
    for d in details:
        ret.append('[🎞](%s) [%s](%s) %s' % (
            d['cover'], d['identifier'],
            CFG['urltemplate'] % (lang, search.base_repr(d['id'], 36)),
            mdescape(d['title'])))
    if len(ret) == 1:
        if d['length']:
            ret.append(mdescape('%s: %smin' % (title_translation['length'][lang], d['length'])))
        for key in ('release', 'director', 'studio', 'label', 'series'):
            if d[key]:
                ret.append(mdescape('%s: %s' % (title_translation[key][lang], d[key])))
        for key in ('tags', 'stars'):
            if d[key]:
                ret.append(mdescape('%s: %s' % (title_translation[key][lang], ', '.join(x[1] for x in d[key]))))
    return '\n'.join(ret)

def inline_result(details, lang='ja'):
    if not details:
        return []
    ret = []
    for d in details:
        ret.append({
            'type': 'article',
            'id': str(d['id']),
            'title': d['identifier'],
            'description': d['title'],
            'input_message_content': {
                'message_text': format_result([d], lang),
                'parse_mode': 'Markdown'
            },
            'thumb_url': d['cover_small'],
            'url': CFG['urltemplate'] % (lang, search.base_repr(d['id'], 36))
        })
    return ret

def handle_api_update(d: dict):
    logger_botapi.debug('Update: %r' % d)
    try:
        if 'inline_query' in d:
            query = d['inline_query']
            text = query['query'].strip()
            if text:
                ret, lang = cmd_query(text)
                r = answer(query['id'], inline_result(ret, lang))
                logger_botapi.debug(r)
        elif 'message' in d:
            msg = d['message']
            text = msg.get('text', '')
            cmd, expr = parse_cmd(text)
            ret = None
            if not expr and not text:
                pass
            elif cmd == 'start':
                ret = 'Send me ID, star, keywords, or advanced query string.'
            elif cmd == 'help':
                ret = '/car - Search Japanese adult movie. Enter ID, star, keywords, or advanced query string'
            elif cmd == 'car' or msg['chat']['type'] == 'private':
                ret = format_result(*cmd_query(expr or text))
            if ret:
                bot_api('sendMessage', chat_id=msg['chat']['id'], text=ret,
                    reply_to_message_id=msg['message_id'], parse_mode='Markdown')
    except Exception:
        logger_botapi.exception('Failed to process a message.')

def load_config():
    return AttrDict(json.load(open('config.json', encoding='utf-8')))

def save_config():
    json.dump(CFG, open('config.json', 'w'), sort_keys=True, indent=1)

if __name__ == '__main__':
    CFG = load_config()
    MSG_Q = queue.Queue()
    WP = search.WhooshProfile(CFG['database'], CFG['indexdir'])
    try:
        updatebotinfo()
        apithr = threading.Thread(target=getupdates)
        apithr.daemon = True
        apithr.start()

        while 1:
            handle_api_update(MSG_Q.get())
    finally:
        save_config()
