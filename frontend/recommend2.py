#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import sys
import sqlite3
import collections
import analyzer

import numpy
import scipy.sparse
import matplotlib.pyplot as plt
import whoosh.lang
import whoosh.lang.porter
from tinysegmenter import TinySegmenter
from sklearn.neighbors import NearestNeighbors
from sklearn.feature_extraction.text import TfidfTransformer

SQL_METADATA_FEATURES = '''
SELECT 'm_' || ((("length" + 30 - 1) / 30) * 30) minutes
FROM movies
WHERE "length" IS NOT NULL
GROUP BY minutes
HAVING count(id) > 2
UNION ALL
SELECT 'y_' || substr(release_date, 1, instr(release_date, '-')-1) relyear
FROM movies
WHERE release_date LIKE '19%' OR release_date LIKE '20%'
GROUP BY relyear
HAVING count(id) > 2
UNION ALL
SELECT 'g_' || min(f.id) fname
FROM (
  SELECT mg.genre id
  FROM movie_genre mg
  INNER JOIN movies m ON m.id=mg.movie
  GROUP BY mg.genre
  HAVING count(m.id) > 2
) q1
INNER JOIN features f ON f.id=q1.id AND f.type='genre'
GROUP BY f.value
UNION ALL
SELECT 's_' || ms.star fname
FROM movie_star ms
INNER JOIN movies m ON m.id=ms.movie
GROUP BY ms.star
HAVING count(m.id) > 2
UNION ALL
SELECT 'dr_' || f.id fname
FROM movies m
INNER JOIN features f ON f.id=m.director AND f.type='director'
GROUP BY f.id
HAVING count(m.id) > 2
UNION ALL
SELECT 'sd_' || f.id fname
FROM movies m
INNER JOIN features f ON f.id=m.studio AND f.type='studio'
GROUP BY f.id
HAVING count(m.id) > 2
UNION ALL
SELECT 'l_' || f.id fname
FROM movies m
INNER JOIN features f ON f.id=m.label AND f.type='label'
GROUP BY f.id
HAVING count(m.id) > 2
UNION ALL
SELECT 'sr_' || f.id fname
FROM movies m
INNER JOIN features f ON f.id=m.series AND f.type='series'
GROUP BY f.id
HAVING count(m.id) > 2
'''

SQL_MOVIE_FEATURES = '''
SELECT
  m.id, coalesce(m.title, ''), m.identifier,
  substr(m.release_date, 1, instr(m.release_date, '-')-1) relyear,
  ((m."length" + 30 - 1) / 30) * 30 minutes,
  group_concat(DISTINCT mg.genre) genre,
  group_concat(DISTINCT ms.star) starids,
  coalesce(group_concat(DISTINCT s.name), '') starnames,
  m.director, m.studio, m.label, m.series
FROM movies m
LEFT JOIN movie_genre mg ON m.id=mg.movie
LEFT JOIN movie_star ms ON m.id=ms.movie
LEFT JOIN stars s ON s.id=ms.star
GROUP BY m.id
'''

class TitleTokenizer():
    def __init__(self):
        self.ts = TinySegmenter()
        self.re_wr = re.compile('\W+')
        self.re_ja = re.compile(
            '([\u3041-\u3096\u30A0-\u30FF'
            '\u3400-\u4dbf\u4e00-\u9fff\uf900-\ufaff'
            '\U00020000-\U0002A6DF\U0002A700-\U0002B73F'
            '\U0002B740-\U0002B81F\U0002B820-\U0002CEAF'
            '\U0002F800-\U0002FA1F]+)'
        )
        self.stoplist = frozenset(
            analyzer.STOP_WORDS_JA | whoosh.lang.stopwords_for_language('en'))

    def __call__(self, text, identifier, stars):
        text = re.sub('\s*%s\s*' % identifier, ' ', text).strip()
        if ' ' in text:
            text = re.sub('\s*(%s)\s*' % '|'.join(' ?'.join(re.escape(ch) for ch in x) for x in stars), ' ', text)
        else:
            text = re.sub('\s*(%s)\s*' % '|'.join(map(re.escape, stars)), ' ', text)
        for tok in self.re_wr.split(text):
            for word in self.ts.tokenize(tok):
                word = word.lower()
                if word not in self.stoplist and (
                    len(word) > 1 or self.re_ja.match(word)):
                    yield whoosh.lang.porter.stem(word)


class Recommender:
    def __init__(self, dbfile='../avsox.db'):
        self.dbfile = dbfile
        self.feature_matrix()

    def feature_matrix(self):
        db = sqlite3.connect(self.dbfile)
        cur = db.cursor()
        ttok = TitleTokenizer()
        mdfeatures = frozenset(row[0] for row in cur.execute(SQL_METADATA_FEATURES))
        prefixes = {}
        keywords = {}
        pfxnum = 0
        kwdnum = 0
        rows = []
        self.rowidx = {}
        self.idxrow = {}
        self.identidx = {}
        self.idxident = {}
        print('Reading...')
        for k, (mid, title, identifier, relyear, minutes, genres, starids, starnames, director, studio, label, series) in enumerate(cur.execute(SQL_MOVIE_FEATURES)):
            feat = set()
            for fmt, val in zip(
                ('m_%s', 'y_%s', 'dr_%s', 'sd_%s', 'l_%s', 'sr_%s'),
                (minutes, relyear, director, studio, label, series)):
                if val:
                    feat.add(fmt % val)
            if genres:
                feat.update('g_' + r for r in genres.split(','))
            if starids:
                feat.update('s_' + r for r in starids.split(','))
                stars = starnames.split(',')
            else:
                stars = tuple()
            feat = feat.intersection(mdfeatures)
            idprefix = 'pf_%s' % analyzer.split_identifier(
                analyzer.regularize_identifier(identifier))[0] or ''
            if idprefix not in prefixes:
                prefixes[idprefix] = pfxnum
                pfxnum += 1
            feat.add(idprefix)
            wcnt = collections.Counter()
            for word in ttok(title, identifier, stars):
                if word not in keywords:
                    keywords[word] = kwdnum
                    kwdnum += 1
                wcnt[word] += 1
            rows.append((tuple(feat), wcnt))
            self.rowidx[mid] = k
            self.idxrow[k] = mid
            self.identidx[identifier] = k
            self.idxident[k] = identifier
        assert len(prefixes) == pfxnum
        assert len(keywords) == kwdnum

        print('dok...')
        columns = keywords.copy()
        for k, v in prefixes.items():
            columns[k] = v + kwdnum
        for v, k in enumerate(sorted(mdfeatures), kwdnum+pfxnum):
            columns[k] = v
        matrix = scipy.sparse.dok_matrix((len(rows), len(columns)), dtype=numpy.float32)
        for i, row in enumerate(rows):
            for key in row[0]:
                matrix[i, columns[key]] = 1
            for word, cnt in row[1].items():
                matrix[i, columns[word]] = cnt
        print('csc...')
        matrix = scipy.sparse.csc_matrix(matrix)
        print('tfidf...')
        tfidf = TfidfTransformer()
        #kwdmtrix = scipy.sparse.csr_matrix(matrix[:,:kwdnum])
        #tagmtrix = scipy.sparse.csr_matrix(matrix[:,kwdnum:])
        #kwdmtrix = tfidf.fit_transform(kwdmtrix)
        tagnz = matrix[:,kwdnum:].nonzero()
        idx1 = tagnz[1]
        idx1 += kwdnum
        matrix[tagnz] *= 3
        print('csr...')
        #matrix = scipy.sparse.hstack((kwdmtrix, tagmtrix), 'csr')
        matrix = scipy.sparse.csr_matrix(tfidf.fit_transform(matrix))
        columns_t = tuple(k for k, v in sorted(columns.items(), key=lambda x: x[1]))
        self.matrix = matrix
        self.columns = columns_t
        self.knn = NearestNeighbors(metric='cosine')
        self.knn.fit(matrix)
        #return matrix, columns_t, rowidx

    def recommend(self, identifier=None, mid=None):
        if identifier:
            rid = self.identidx[identifier]
        else:
            rid = self.rowidx[mid]
        dist, ind = self.knn.kneighbors(self.matrix[rid], 10)
        for d, i in zip(dist, ind):
            print('%s %.6f' % (self.idxident[int(i)], d))

feature_order = {
    'genre': 1,
    'director': 2,
    'studio': 3,
    'label': 4,
    'series': 5,
    'movie': 6,
    'star': 7
}
selected_features = set(('idprefix', 'length', 'genre',
    'director', 'studio', 'label', 'series', 'star'))
# selected_features = set(('idprefix', 'genre'))
#selected_features = set(('genre',))


'''
SELECT
title,
substr(identifier, 1, instr(identifier, '-')-1) idprefix,
substr(release_date, 1, instr(release_date, '-')-1) relyear,
(("length" + 30 - 1) / 30) * 30 minutes,
director, studio, label, series
FROM movies
LEFT JOIN movie_star

'''



def map_features():
    '''
    Stats:
    feature   count   blank
    =======================
    idprefix  12470       0
    length      (*)     916
    -----------------------
    genre       299     429
    director   4748  182365
    studio     4400     439
    label      8810    5824
    series    31618  132549
    -----------------------
    star      37817  121708
    -----------------------
    [total]   87692
    [movie count]    312034

    (*) min/ max/avg/stdev
          1/2880/151/104
    '''
    features = {}
    count = int('length' in selected_features)
    if 'idprefix' in selected_features:
        idprefixes = set()
        for row in CUR.execute('SELECT identifier FROM movies'):
            idprefixes.add(split_identifier(row[0])[0])
        for pfx in idprefixes:
            features[('idprefix', pfx)] = count
            count += 1
    for ftype, fid in CUR.execute('SELECT type, id FROM features'):
        if ftype in selected_features:
            features[(ftype, fid)] = count
            count += 1
    if 'star' in selected_features:
        for row in CUR.execute('SELECT id FROM stars'):
            features[('star', row[0])] = count
            count += 1
    return features

def movie_matrix(features):
    number = CUR.execute('SELECT count(*) FROM movies').fetchone()[0]
    ids = {}
    matrix = numpy.zeros((number, len(features)), numpy.float32)
    for k, row in enumerate(CUR.execute(
        'SELECT id, identifier, length, director, studio, label, series FROM movies')):
        mid, identifier, length, director, studio, label, series = row
        ids[mid] = k
        if 'idprefix' in selected_features:
            matrix[k][features[('idprefix', split_identifier(identifier)[0])]] = 1
        if 'length' in selected_features:
            matrix[k][0] = int(length / 2880 * 255)
        if 'director' in selected_features and director:
            matrix[k][features[('director', director)]] = 1
        if 'studio' in selected_features and studio:
            matrix[k][features[('studio', studio)]] = 1
        if 'label' in selected_features and label:
            matrix[k][features[('label', label)]] = 1
        if 'series' in selected_features and series:
            matrix[k][features[('series', series)]] = 1
    if 'genre' in selected_features:
        for movie, genre in CUR.execute('SELECT movie, genre FROM movie_genre'):
            matrix[ids[movie]][features[('genre', genre)]] = 1
    if 'star' in selected_features:
        for movie, star in CUR.execute('SELECT movie, star FROM movie_star'):
            matrix[ids[movie]][features[('star', star)]] = 1
    return ids, matrix

def pca(X, dim):
    X -= numpy.mean(X, 0, dtype=numpy.float32)
    # Sigma = numpy.cov(X.T)
    Sigma = X.T.dot(X)
    Sigma *= 1. / numpy.float32(X.shape[0])
    print(Sigma.shape)
    U, S, V = numpy.linalg.svd(Sigma, full_matrices=False)
    # return U[:, :dim] * S[:dim]
    return U, S

if __name__ == "__main__":
    #features = map_features()
    #print(features)
    #ids, matrix = movie_matrix(features)

    #print(len(features))
    #print(len(ids))
    #print(matrix.shape)

    '''
    fmx, col = feature_matrix()


    #numpy.save('movies.npy', matrix)

    #import scipy.io
    #scipy.io.savemat('movies.mat', dict(X=matrix))

    U, S = pca(matrix, 264)
    reduced = U[:, :100] * S[:100]
    print(reduced.shape)
    #assert matrix == reduced
    #print(pca(matrix, 200).shape)


    sel = (numpy.random.rand(1000) * matrix.shape[0]).astype(numpy.int32)
    print(sel)
    Xred = matrix.dot(U[:, :2])
    print(Xred.shape)
    x, y = Xred.T[:, sel]
    plt.scatter(x, y, edgecolors='none')

    plt.grid(True)
    plt.show()
    '''
    rec = Recommender(sys.argv[1])
    for ln in sys.stdin:
        rec.recommend(ln.strip())
