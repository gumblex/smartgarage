#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import json
import sqlite3
import logging
import argparse
import datetime
import readline
import itertools
import functools

import jieba
import zhconv
import whoosh.index
import whoosh.query
import whoosh.qparser
import whoosh.scoring
import whoosh.sorting
import whoosh.highlight
import whoosh.analysis
from whoosh.fields import *
from whoosh.filedb.filestore import FileStorage

import analyzer

logging.basicConfig(stream=sys.stderr, format='%(asctime)s [%(levelname)s] %(message)s', level=logging.DEBUG)

SQL_GET_RECORDS = '''
SELECT
  m.id,
  coalesce(title.value, m.title) title,
  m.identifier,
  m.release_date,
  coalesce(director_t.value, director.value) director,
  coalesce(studio_t.value, studio.value) studio,
  coalesce(label_t.value, label.value) label,
  coalesce(series_t.value, series.value) series,
  g.tags,
  s.star
FROM movies m
LEFT JOIN (
  SELECT
    movie,
    group_concat(coalesce(genres_t.value, genres.value)) tags
  FROM movie_genre
  LEFT JOIN features genres ON genres.id=genre AND genres.type='genre'
  LEFT JOIN translations genres_t
    ON genres_t.id=genre AND genres_t.type='genre' AND genres_t.lang=?
  GROUP BY movie
  ) g
  ON m.id=g.movie
LEFT JOIN (
  SELECT
    movie,
    group_concat(coalesce(stars_t.value, stars.name)) star
  FROM movie_star
  LEFT JOIN stars ON stars.id=star
  LEFT JOIN translations stars_t
    ON stars_t.id=star AND stars_t.type='star' AND stars_t.lang=?
  GROUP BY movie
  ) s
  ON m.id=s.movie
LEFT JOIN
  translations title
  ON title.id=m.id AND title.type='movie' AND title.lang=?
LEFT JOIN
  features director
  ON director.id=m.director AND director.type='director'
LEFT JOIN
  translations director_t
  ON director_t.id=m.director AND director_t.type='director' AND director_t.lang=?
LEFT JOIN
  features studio
  ON studio.id=m.studio AND studio.type='studio'
LEFT JOIN
  translations studio_t
  ON studio_t.id=m.studio AND studio_t.type='studio' AND studio_t.lang=?
LEFT JOIN
  features label
  ON label.id=m.label AND label.type='label'
LEFT JOIN
  translations label_t
  ON label_t.id=m.label AND label_t.type='label' AND label_t.lang=?
LEFT JOIN
  features series
  ON series.id=m.series AND series.type='series'
LEFT JOIN
  translations series_t
  ON series_t.id=m.series AND series_t.type='series' AND series_t.lang=?
'''

SQL_GET_ONE_RECORD = '''
SELECT
  m.id,
  coalesce(title.value, m.title) title,
  m.identifier,
  m.release_date,
  m.length,
  coalesce(director_t.value, director.value) director,
  coalesce(studio_t.value, studio.value) studio,
  coalesce(label_t.value, label.value) label,
  coalesce(series_t.value, series.value) series,
  m.cover,
  m.cover_small
FROM movies m
LEFT JOIN
  translations title
  ON title.id=m.id AND title.type='movie' AND title.lang=?
LEFT JOIN
  features director
  ON director.id=m.director AND director.type='director'
LEFT JOIN
  translations director_t
  ON director_t.id=m.director AND director_t.type='director' AND director_t.lang=?
LEFT JOIN
  features studio
  ON studio.id=m.studio AND studio.type='studio'
LEFT JOIN
  translations studio_t
  ON studio_t.id=m.studio AND studio_t.type='studio' AND studio_t.lang=?
LEFT JOIN
  features label
  ON label.id=m.label AND label.type='label'
LEFT JOIN
  translations label_t
  ON label_t.id=m.label AND label_t.type='label' AND label_t.lang=?
LEFT JOIN
  features series
  ON series.id=m.series AND series.type='series'
LEFT JOIN
  translations series_t
  ON series_t.id=m.series AND series_t.type='series' AND series_t.lang=?
WHERE m.id=?
'''

SQL_GET_TAGS = '''
SELECT genre, coalesce(genres_t.value, genres.value) name FROM movie_genre
LEFT JOIN features genres ON genres.id=genre AND genres.type='genre'
LEFT JOIN translations genres_t
  ON genres_t.id=genre AND genres_t.type='genre' AND genres_t.lang=?
WHERE movie=?
'''

SQL_GET_STARS = '''
SELECT star, coalesce(stars_t.value, stars.name) name FROM movie_star
LEFT JOIN stars ON stars.id=star
LEFT JOIN translations stars_t
  ON stars_t.id=star AND stars_t.type='star' AND stars_t.lang=?
WHERE movie=?
'''

re_alphanum = re.compile('^([A-Z0-9]+?)[ _]?([0-9]+)$')

def split_identifier(identifier):
    res = tuple(identifier.split('-', 1))
    if len(res) == 1:
        return None, res[0]
    else:
        return res

def regularize_identifier(identifier):
    idn = identifier.strip().upper()
    pfx, num = split_identifier(idn)
    if pfx is None:
        match = re_alphanum.match(idn)
        if match:
            pfx, num = match.groups()
        print(idn, pfx, num)
    if pfx is None:
        return num
    else:
        return pfx + '-' + num

def base_repr(number, base=2, padding=0):
    """
    Return a string representation of a number in the given base system.
    Parameters
    ----------
    number : int
        The value to convert. Positive and negative values are handled.
    base : int, optional
        Convert `number` to the `base` number system. The valid range is 2-36,
        the default value is 2.
    padding : int, optional
        Number of zeros padded on the left. Default is 0 (no padding).
    Returns
    -------
    out : str
        String representation of `number` in `base` system.
    See Also
    --------
    binary_repr : Faster version of `base_repr` for base 2.
    Examples
    --------
    >>> np.base_repr(5)
    '101'
    >>> np.base_repr(6, 5)
    '11'
    >>> np.base_repr(7, base=5, padding=3)
    '00012'
    >>> np.base_repr(10, base=16)
    'A'
    >>> np.base_repr(32, base=16)
    '20'

    Copied from `numpy.base_repr`.
    """
    # digits = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    digits = '0123456789abcdefghijklmnopqrstuvwxyz'
    if base > len(digits):
        raise ValueError("Bases greater than 36 not handled in base_repr.")
    elif base < 2:
        raise ValueError("Bases less than 2 not handled in base_repr.")

    num = abs(number)
    res = []
    while num:
        res.append(digits[num % base])
        num //= base
    if padding:
        res.append('0' * padding)
    if number < 0:
        res.append('-')
    return ''.join(reversed(res or '0'))

class WhooshProfile:

    def __init__(self, db, path, website=None):
        self.path = path
        self.website = website or os.path.splitext(os.path.basename(db))[0]
        if self.website in ('avmoo', 'avsox'):
            self.languages = ('ja', 'en', 'cn', 'tw')
        elif self.website == 'avmemo':
            self.languages = ('en', 'cn', 'ja', 'tw')
        else:
            raise ValueError('unknown website name: ' + self.website)
        self.db = sqlite3.connect(db)
        self.schema = Schema(
            id=NUMERIC(int, 32, signed=False, stored=True, unique=True, field_boost=0),
            title=TEXT(analyzer=analyzer.EnJaAnalyzer()),
            identifier=ID(analyzer=analyzer.IdentifierAnalyzer()),
            release=DATETIME(sortable=True),
            director=KEYWORD(lowercase=True, commas=True),
            studio=KEYWORD(lowercase=True, commas=True),
            label=KEYWORD(lowercase=True, commas=True),
            series=KEYWORD(lowercase=True, commas=True),
            tag=KEYWORD(lowercase=True, commas=True, scorable=True),
            star=KEYWORD(lowercase=True, commas=True, scorable=True)
        )

    def create(self):
        logging.info('Creating schema at %s...', self.path)
        if not os.path.exists(self.path):
            os.mkdir(self.path)
        storage = FileStorage(self.path)
        ix = storage.create_index(self.schema, indexname=self.website)
        logging.info('Done.')
        return ix

    def make_index(self):
        cur = self.db.cursor()

        @functools.lru_cache(maxsize=65536)
        def get_feature_multi(ftype, fid):
            if fid is None:
                return None
            elif ftype == 'movie':
                return [r[0] for r in cur.execute("SELECT DISTINCT value FROM translations WHERE type=? AND id=?", (ftype, fid))]
            else:
                translations = set(r[0] for r in cur.execute("SELECT DISTINCT value FROM translations WHERE type=? AND id=?", (ftype, fid)))
            # this only gets translations for 'movie'
            if ftype == 'star':
                res = cur.execute('SELECT name FROM stars WHERE id=?', (fid,)).fetchone()
                if res:
                    translations.add(res[0])
            else:
                translations.add(cur.execute(
                    'SELECT value FROM features WHERE type=? AND id=?',
                    (ftype, fid)).fetchone()[0])
            return tuple(translations)
        commajoin = lambda x: None if x is None else ','.join(x)

        logging.info('Indexing...')
        storage = FileStorage(self.path)
        try:
            ix = storage.open_index(indexname=self.website)
        except whoosh.index.EmptyIndexError:
            ix = self.create()
        writer = ix.writer()
        indexed = set()
        rows = []
        with ix.searcher() as searcher:
            for fields in searcher.all_stored_fields():
                indexed.add(fields['id'])
        for row in cur.execute('SELECT id, title, identifier, release_date, director, studio, label, series FROM movies'):
            if row[0] not in indexed:
                rows.append(row)
        del indexed
        for k, row in enumerate(rows):
            mid, title, identifier, release, director, studio, label, series = row
            release_dt = None
            if release:
                try:
                    release_dt = datetime.datetime.strptime(release, '%Y-%m-%d')
                except ValueError:
                    # 0000-00-00
                    pass
            tags = ','.join(analyzer.split_tags_uniq(itertools.chain.from_iterable(
                    get_feature_multi('genre', r[0]) for r in cur.execute(
                    'SELECT genre FROM movie_genre WHERE movie=?',
                    (mid,)).fetchall())))
            stars = ','.join(itertools.chain.from_iterable(
                    get_feature_multi('star', r[0]) for r in cur.execute(
                    'SELECT star FROM movie_star WHERE movie=?',
                    (mid,)).fetchall()))
            attrs = {
                'id': mid,
                'title': ' '.join(analyzer.uniq([title] +
                         get_feature_multi('movie', mid))),
                'identifier': identifier,
                'release': release_dt,
                'director': commajoin(get_feature_multi('director', director)),
                'studio': commajoin(get_feature_multi('studio', studio)),
                'label': commajoin(get_feature_multi('label', label)),
                'series': commajoin(get_feature_multi('series', series)),
                'tag': tags,
                'star': stars
            }
            writer.add_document(**attrs)
            if k % 1000 == 0:
                logging.info('Indexed: %d', k)
                #logging.debug(get_feature_multi.cache_info())
        writer.commit()
        logging.info('Done.')
        ix.close()

    def search(self, userquery=None, sort=True, page=1, limit=25):
        storage = FileStorage(self.path)
        ix = storage.open_index(indexname=self.website)
        parser = whoosh.qparser.QueryParser(None, ix.schema)
        parser.add_plugin(whoosh.qparser.MultifieldPlugin(
            ('identifier', 'title', 'star'), group=whoosh.qparser.syntax.OrGroup))
        parser.add_plugin(whoosh.qparser.FieldAliasPlugin({
            "tag": ("genre",), "star": ("actress",)
        }))
        facet = whoosh.sorting.FieldFacet("release")
        # work around release is None
        facet = whoosh.sorting.TranslateFacet(lambda x: -x if x != 2**64-1 else 0, facet)
        with ix.searcher() as searcher:
            if userquery:
                query = parser.parse(userquery)
                results = searcher.search_page(query, page, pagelen=limit,
                    sortedby=(facet if sort else None))
                pagecount = results.pagecount
                total = results.total
            else:
                results = searcher.search_page(whoosh.query.Every(), page,
                    pagelen=limit, sortedby=facet)
                pagecount = results.pagecount
                total = results.total
            results = [h['id'] for h in results]
        return results, pagecount, total

    def expand_results(self, results, lang='ja'):
        cur = self.db.cursor()
        result_dicts = []
        for hit in results:
            (mid, title, identifier, release, length,
             director, studio, label, series, cover, cover_small) = \
                cur.execute(SQL_GET_ONE_RECORD, (lang,)*5 + (hit,)).fetchone()
            tags = cur.execute(SQL_GET_TAGS, (lang, hit)).fetchall()
            stars = cur.execute(SQL_GET_STARS, (lang, hit)).fetchall()
            result_dicts.append({
                'id': mid,
                'title': title,
                'identifier': identifier,
                'release': release,
                'length': length,
                'director': director,
                'studio': studio,
                'label': label,
                'series': series,
                'tags': tags,
                'stars': stars,
                'cover': cover,
                'cover_small': cover_small
            })
        return result_dicts

    def show_results_text(self, results, lang='ja'):
        cur = self.db.cursor()
        for d in self.expand_results(results, lang):
            print('%s %s %s' % (base_repr(d['id'], 36), d['identifier'], d['title']))
            for key in ('length', 'release', 'director', 'studio', 'label', 'series'):
                if d[key]:
                    print('    %s: %s' % (key.capitalize(), d[key]))
            for key in ('tags', 'stars'):
                if d[key]:
                    print('    %s: %s' % (key.capitalize(), ', '.join(x[1] for x in d[key])))

def main():
    parser = argparse.ArgumentParser(description='Manage movie index.')
    parser.add_argument('-u', '--update', action='store_true', help='update index')
    parser.add_argument('-r', '--recreate', action='store_true', help='recreate index')
    parser.add_argument('-w', '--website', help='set website name explicitly')
    parser.add_argument('-l', '--lang', default='ja', help='set display language')
    parser.add_argument('database', help='database file name')
    parser.add_argument('indexdir', help='index directory name')
    args = parser.parse_args()
    wp = WhooshProfile(args.database, args.indexdir, args.website)
    if args.recreate:
        wp.create()
    if args.update:
        wp.make_index()
    else:
        while 1:
            try:
                linein = input('> ')
            except (EOFError, KeyboardInterrupt):
                break
            results, pagecount, total = wp.search(linein)
            print('Found %s results, showing page 1' % total)
            wp.show_results_text(results, args.lang)

if __name__ == '__main__':
    main()
